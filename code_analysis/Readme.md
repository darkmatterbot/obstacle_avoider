# AMR Code Analysis

## Inspired by https://github.com/platisd/duplicate-code-detection-tool

## Requirements
  - python3.8
  - pip3
  - gensim
  - nltk and nltk.punkt

## Installation
  - nltk and gensim
  ```
  pip3 install nltk gensim
  ```
  - nltk.punkt
  ```
  python3 -m nltk.downloader punkt
  ```

### Usage

```
python3 duplicate_analysis.py -d /Repos --ignore-threshold 20
```
