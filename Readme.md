# Finalproject - Collecting Goalpoints
#### A Naive Obstacle Avoidance Technique for Turtlebot 3 implemented in ROS

## Introduction
The goal of this project is to approach multiple targets while avoiding obstacles.

## Content
   - The *.img* folder contains screenshots, illustrations and diagrams

   - The *final_project_practice* folder contains the projects-world `model.sdf` which is placed in gazebo and the *practice_goals.yaml* file houses several goalpoints for training purposes.

   - In the *jm_191091_prj* folder is the rosnode containing the algorithm that allows the robot to approach target-goalpoints while avoiding obstacles.

   - The *tests* folder contains .sh scripts which are called from the *.gitlab-ci.yaml* to check if the node can be launched and whether or not the repository shall be cloned.

   - The *presentation* folder contains a [link](https://docs.google.com/presentation/d/1bA88j5DA12nlxuIMUOZV1iHxTsp45P4jWNU9sGUJ1JE/edit#slide=id.p) to a google presentation about this project.



## Pre-requisites
- Python 2 or 3
- Gazebo (comes pre-installed with ros-desktop-full)
- [Turtlebot 3 simulation package](https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git)

## Installation
```
cd ~/catkin_ws/src
git clone https://fbe-gitlab.hs-weingarten.de/stud-amr/2020-ws-bachelor/jm-191091_tier4
cd ~/catkin_ws
catkin_make
. devel/setup.bash
```

## Usage
```
roslaunch turtlebot3_gazebo turtlebot3_empty_world.launch
rosrun gazebo_ros spawn_model -file /home/joni/catkin_ws/src/jm-191091_tier4/final_project_practice/model.sdf -sdf -x 2 -y 1 -model final_project
roslaunch goal_publisher goal_publisher.launch config_file:=/home/joni/catkin_ws/src/jm-191091_tier4/final_project_practice/practice_goals.yaml
roslaunch xx_123456_prj start.launch
```

Note that at this point, the robot does not move, only the action server is up and running. `rostopic list` should yield:
```
/final_action_server/cancel
/final_action_server/feedback
/final_action_server/goal
/final_action_server/result
/final_action_server/status
```

The robot can be started by publishing into the action servers provided goal-topic (use double-tab for parameter autocompletion):
```
rostopic pub /final_action_server/goal action/StartActionGoal "header:
seq: 0
stamp:
secs: 0
nsecs: 0
frame_id: ''
goal_id:
stamp:
secs: 0
nsecs: 0
id: ''
goal:
start_driving: true"
```
Now the robot should start moving, hunting for goalpoints.
We can then randomly choose to get the robot to stop and turn any given amount of degrees:
```
rosservice call /rotate_now "degrees: 360"
```

## Details
The code inside the `src` folder already has necessary comments to understand what's going on. We define a `final_project` node which subscribes to the following topics:
  - **`/gazebo/model_states`** for a precise position and rotation
  - **`/goals`** for fetching the target-points
  - **`/scan`** for obstacle detection

The node publishes Twist() messages into the
  - **`/cmd_vel`** topic to manipulate turtlebots linear and angular velocity.
</br>
</br>
We slice up laserscan data into approptiate sections to dynamically extract relevant data.
<img src=".img/slice.png" height="390" width="750">

## Algorithm
<img src=".img/Obstacle_avoider.png" height="auto" width="auto">

## Implementation
The angular velocity is always equal to the calculated error angle relative to the goalpoint, leading to fast rotation when the error is high, and slow or no rotation when the error is low or zero.
<img src=".img/angular_rot.png" height="auto" width="auto">
<img src=".img/obstacle_avoidance.png" height="auto" width="auto">
</br></br>
The linear velocity is relative to the distance of the closest obstacle, de- or increasing in proportion to the distance to the obstacle.
<img src=".img/infront.png" height="auto" width="auto">
<img src=".img/left_right.png" height="auto" width="auto">
<img src=".img/anti_drift.png" height="auto" width="auto">

## Problems and Solutions
This algorithm does not use path finding or object assignment. In certain situations, this can lead to loops where targets are behind the wall and the robot repeatedly takes the "wrong" turn, causing it to repeatedly go back and forth with significant time overhead. This time-based loop detection is inspired by Jacob Zeising (jz_171702) and can guide the robot out of this miserable situation.
</br>
<img src=".img/problem_solution.png" height="auto" width="auto">
</br></br>

If the robot gets too close to an obstacle, it shifts into reverse. This mechanism is inspired by Leon Baumann (lb-191042).

### Note
This is implemented on Ubuntu 20.04, ROS Noetic

## To remove ros from your system
   - sudo apt-get remove ros-*
   - sudo apt-get autoremove
   - rm /etc/apt/sources.list.d/ros-latest.list

## Author
  jm-191091

## Reference
1. http://wiki.ros.org/ROS/Tutorials
2. https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
3. http://gazebosim.org/tutorials?tut=ros_overview
4. https://www.theconstructsim.com/ros-qa-053-how-to-move-a-robot-to-a-certain-point-using-twist/
